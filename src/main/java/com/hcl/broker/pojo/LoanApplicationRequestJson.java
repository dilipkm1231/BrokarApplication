package com.hcl.broker.pojo;

public class LoanApplicationRequestJson {
	private double amount;
	private String loantype;
	private double monthlyexpense;
	private double monthlyincome;
	private int loanterm;
	private int repaymentterm;
	private int repaymentfrequency;
	private String borrowername;
	private String phone;
	private String addr1;
	private String aadr2;
	private String city;
	private int pincode;
	private String stae;
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public String getLoantype() {
		return loantype;
	}
	public void setLoantype(String loantype) {
		this.loantype = loantype;
	}
	public double getMonthlyexpense() {
		return monthlyexpense;
	}
	public void setMonthlyexpense(double monthlyexpense) {
		this.monthlyexpense = monthlyexpense;
	}
	public double getMonthlyincome() {
		return monthlyincome;
	}
	public void setMonthlyincome(double monthlyincome) {
		this.monthlyincome = monthlyincome;
	}
	public int getLoanterm() {
		return loanterm;
	}
	public void setLoanterm(int loanterm) {
		this.loanterm = loanterm;
	}
	public int getRepaymentterm() {
		return repaymentterm;
	}
	public void setRepaymentterm(int repaymentterm) {
		this.repaymentterm = repaymentterm;
	}
	public int getRepaymentfrequency() {
		return repaymentfrequency;
	}
	public void setRepaymentfrequency(int repaymentfrequency) {
		this.repaymentfrequency = repaymentfrequency;
	}
	public String getBorrowername() {
		return borrowername;
	}
	public void setBorrowername(String borrowername) {
		this.borrowername = borrowername;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getAddr1() {
		return addr1;
	}
	public void setAddr1(String addr1) {
		this.addr1 = addr1;
	}
	public String getAadr2() {
		return aadr2;
	}
	public void setAadr2(String aadr2) {
		this.aadr2 = aadr2;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public int getPincode() {
		return pincode;
	}
	public void setPincode(int pincode) {
		this.pincode = pincode;
	}
	public String getStae() {
		return stae;
	}
	public void setStae(String stae) {
		this.stae = stae;
	}


	
}
